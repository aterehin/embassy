/**
 * Third party
 */
//= ../../../bower_components/jquery/dist/jquery.min.js
//= ../../../bower_components/owl.carousel/dist/owl.carousel.min.js
//= ../../../bower_components/fancybox/source/jquery.fancybox.pack.js

/**
 * Custom
 */
jQuery(document).ready(function ($) {

    //= partials/video-popup.js
    //= partials/search-popup.js
    //= partials/news-slider.js
    //= partials/partners.js
    //= partials/read-more.js
    //= partials/clear-input.js
    //= partials/mobile-menu.js
    //= partials/nav-list.js
    //= partials/more-news.js
    //= partials/nav-tabs.js

});
