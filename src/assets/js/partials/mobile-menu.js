/**
 * Clear Input
 */

(function() {

	"use strict";

    var $openBtn = $('[data-toggle="menu-open"]');
    var $closeBtn = $('[data-toggle="menu-close"]');
    var $menu = $('[data-html="mobile-menu"]');

    $(window).on('load', function(){
        $openBtn.on('click', function(e){
            e.preventDefault();

            if ($menu.not(':visible')) {
                $menu.fadeIn(350);
                $closeBtn.fadeIn(350);
            }
        });

        $closeBtn.on('click', function(e){
            e.preventDefault();

            $menu.fadeOut(300);
            $(this).fadeOut(300);
        });
    })

})();
