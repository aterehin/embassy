/**
 * News Slider
 */

(function () {

    "use strict";

    var slider, sliderThumbs;

    var $slider = $('.n-slider');
    var $sliderThumbs = $slider.closest('.n-slider_thumbs').find('.n-slider_thumbs_wrap');
    var $tabs = $slider.closest('.n-slider_tabs').find('.n-slider_tabs_item');

    var classes = {
        active: '__active'
    }

    var userData = {
        autoplay: function () {
            return $slider.is('[data-autoplay]');
        },
        autoplayTimeout: function () {
            return this.getSpeed(5000, 'autoplay');
        },
        smartSpeed: function () {
            return this.getSpeed(250, 'speed');
        },
        getSpeed: function (speed, selector) {
            var data = $slider.data(selector);

            if (data > 0) {
                speed = data;
            }

            return speed;
        }
    }

    var options = {
        nav: true,
        mouseDrag: false,
        navText: ['<i class="icon-n-slider-prev"></i>', '<i class="icon-n-slider-next"></i>']
    }

    slider = $slider
        .on('initialized.owl.carousel', handleSliderInit)
        .owlCarousel($.extend({}, {
            loop: true,
            items: 1,
            autoplay: userData.autoplay(),
            autoplayTimeout: userData.autoplayTimeout(),
            autoplayHoverPause: true,
            smartSpeed: userData.smartSpeed(),
            animateOut: 'fadeOut',
            responsive: {
                0: {
                    autoHeight: true
                },
                768: {
                    autoHeight: false
                }
            }
        }, options))
        .data('owl.carousel');

    sliderThumbs = $sliderThumbs
        .on('initialized.owl.carousel', handleThumbsInit)
        .owlCarousel($.extend({}, {
            items: 6,
            margin: 15
        }, options))
        .data('owl.carousel');

    $slider
        .on('change.owl.carousel', handleSliderChange);

    $tabs
        .on('mouseenter', handleMouseEnter)
        .on('mouseleave', handleMouseLeave)
        .on('click', handleTabClick);

    $sliderThumbs
        .on('mouseenter', handleMouseEnter)
        .on('mouseleave', handleMouseLeave)
        .on('click', '.owl-item', handleThumbClick);

    function handleSliderInit() {
        changeTab(0);
    }

    function handleThumbsInit() {
        changeThumb(0);
    }

    function handleSliderChange(e) {
        var index = slider.relative(e.property.value);

        changeTab(index);
        changeThumb(index);
    }

    function handleTabClick() {
        var index = $(this).index();

        changeSlide(index);
    }

    function handleThumbClick() {
        var index = sliderThumbs.relative($(this).index());

        changeSlide(index);
    }

    function handleMouseEnter() {
        slider._plugins.autoplay.pause();
    }

    function handleMouseLeave() {
        slider._plugins.autoplay.play();
    }

    function changeTab(index) {
        $tabs
            .removeClass(classes.active)
            .eq(index)
            .addClass(classes.active);
    }

    function changeThumb(index) {
        $sliderThumbs
            .find('.owl-item')
            .removeClass(classes.active)
            .eq(index)
            .addClass(classes.active)
            .trigger('to.owl.carousel', [index, options.smartSpeed, true]);
    }

    function changeSlide(index) {
        $slider
            .trigger('to.owl.carousel', [index, options.smartSpeed, true]);
    }

})();
