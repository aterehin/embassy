/**
 * Read More
 */

(function() {

	"use strict";

    var $link = $('[data-toggle="read-more"]');

    $(window).on('load', function(){
        $link.on('click', function(e){
            e.preventDefault();
            var $this = $(this);

            $this.next('.hidden-area')
                .fadeIn(400);
            $this.fadeOut(0);
        })
    })

})();
