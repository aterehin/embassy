/**
 * Read More
 */

(function() {

	"use strict";

    $(document).ready(function(){
        var $tabs = $('[data-target]');
        var $entry = $('[data-entry]');

        $tabs.on('click', function(e){
            console.log(this);
            e.preventDefault();
            var $this = $(this);

            if (!$this.parent('li').hasClass('__active')) {
                $tabs.parent('li')
                    .removeClass('__active');
                $this.parent('li')
                    .addClass('__active');

                $entry.fadeOut(0);
                $('[data-entry="' + $this.data('target') + '"]')
                    .fadeIn(250);
            }
        })
    })

})();
