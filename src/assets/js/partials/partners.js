/**
 * Partners
 */

(function() {

	"use strict";

	var slider = $('.p-slider');
	var options = {
		items: 6,
        margin: 0,
		loop: true,
		nav: true,
		autoWidth: false,
		navText: ['<i class="icon-partners-nav-prev"></i>', '<i class="icon-partners-nav-next"></i>'],
        responsive: {
            0: {
                items: 3
            },
            768: {
                items: 5
            },
            991: {
                items: 6
            }
        }
	};

	$(window).on('load', init);

	function init() {
		slider.owlCarousel(options);
	}

})();