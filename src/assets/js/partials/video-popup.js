/**
 * Video Popup
 */

(function() {

	"use strict";

	var $popupBtn = $('[data-fancybox-group]');
	var options = {
		type: 'ajax',
		fitToView: false,
		padding: 0
	};

	$popupBtn.fancybox(options);

})();
