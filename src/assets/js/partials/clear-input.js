/**
 * Clear Input
 */

(function() {

	"use strict";

    var $link = $('[data-toggle="clear-input"]');

    $(window).on('load', function(){
        $link.on('click', function(e){
            e.preventDefault();
            var $this = $(this);

            $this.closest('form')
                .find('input[type="text"]')
                .val('')
                .focus()
        })
    })

})();
