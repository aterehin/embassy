/**
 * More news
 */

(function() {

	"use strict";

    var $btn = $('[data-toggle="more-news"]');
    var $list = $('[data-js="news-list"]');

    $(window).on('load', function(){
        $btn.on('click', function(e){
            e.preventDefault();
            var $this = $(this);

            if ($this.hasClass('__active')) {
                $this.removeClass('__active')
                    .text('Больше новостей');

                $list.addClass('__preview')
                    .removeClass('__all')
            } else {
                $this.addClass('__active')
                    .text('Меньше новостей');

                $list.addClass('__all')
                    .removeClass('__preview')
            }
        })
    })

})();
