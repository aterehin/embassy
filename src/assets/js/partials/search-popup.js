/**
 * Search Popup
 */

(function() {

	"use strict";

    var $btn = $('[data-search]');
    var $popup = $('.search-form__popup');

    $(window).on('load', function(){
        $btn.on('click', function(e){
            e.preventDefault();
            var $this = $(this);

            if ($this.attr('data-search') == 'hide') {
                $popup.fadeOut(200)
            } else {
                $popup.fadeIn(200);
                $popup.find('input[type="text"]')
                    .focus();
            }
        })
    })

})();
