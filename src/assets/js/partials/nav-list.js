/**
 * Clear Input
 */

(function() {

	"use strict";

    var $btn = $('[data-toggle="open-list"]');

    $(window).on('load', function(){
        $btn.on('click', function(e){
            e.preventDefault();
            var $this = $(this);

            if ($this.hasClass('current-item__active')) {
                $this.siblings('li')
                    .hide(0);
                $this.removeClass('current-item__active');
            } else {
                $this.siblings('li')
                    .show(0);
                $this.addClass('current-item__active');
            }
        })
    })

})();
